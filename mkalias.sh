#!/bin/bash

errorInfo() {
  echo "For information on how to use 'mkalias' see a man page -> 'man mkalias'"
}

if [ $# -gt 0 ]
  then
  
  # save reference to $OLDPWD
  OLDPWDref=$OLDPWD

  version="1.1"

  # file in which aliases are added
  file1=".bash_aliases"

  file0=".bashrc"

  ##### functions #####

  mkinstall() {
    sudo cp "$OLDPWD"/mkalias.sh /usr/local/bin/scripts
    echo "'mkalias.sh' added to '/usr/local/bin/scripts'"
    sudo gzip -k "$OLDPWD"/mkalias.1
    sudo cp "$OLDPWD"/mkalias.1.gz /usr/local/man/man1
    sudo rm "$OLDPWD"/mkalias.1.gz
    echo "'mkalias' man page added to '/usr/local/man/man1'"
    souceFile
    echo "'mkalias' installed/updated successfully!"
    echo "To get familiar with 'mkalias' see 'man mkalias'."
  }

  add() {
    echo alias $1=\'"$2"\' >> $file1
  }

  delete() {
    grep -v $1= $file1 > temp && mv temp $file1
    unalias $1 >/dev/null 2>/dev/null
  }

  update-value() {
    delete $1
    add $1 "$2"
  }

  update-key() {
    oldAlias=$(grep $1= $file1)
    newAlias="${oldAlias/$1/$2}"
    delete $1
    echo $newAlias >> $file1
  }

  # checks if string is present in file
  check() {
    if [ "$1" ]
      then
      if grep -qF "$1" $2
        then
        return 0
      else 
        return 1
      fi
    fi
  }

  souceFile() {
    if [ -f $file0 ]
      then
      . $file0
    fi
  }

  createFile() {
    touch $file1
    echo "File '$file1' created."
  }

  # first go to the home folder since there are files we want to write to
  cd ~

  # create $file1 if it doesn't exist
  if [ ! -f $file1 ]
    then
    createFile
  fi

  # import $file1 in $file2
  if [ ! -f $file0 ]
    then
      echo "Error! File '$file0' doesn't exist. You will have to manually create it and source '$file1' from within it."
    else
      if ! check $file1 $file0
        then
        echo >> $file0
        echo "# source $file1" >> $file0
        echo "if [ -f ~/$file1 ]" >> $file0
        echo "  then" >> $file0
        echo "  . ~/$file1" >> $file0
        echo "fi" >> $file0
      fi
  fi

  if [ $1 = "--install" ]
    then # install alias for a script
    if [ $# -eq 1 ]
      then
      if check "alias mkalias=" $file1
        then
        echo "'mkalias' is already installed!"
        echo "Updating!"
        mkinstall
      else
        if [ ! -d /usr/local/bin/scripts ]
          then
          sudo mkdir /usr/local/bin/scripts
        fi
        if [ ! -d /usr/local/man/man1 ]
          then
          sudo mkdir /usr/local/man/man1
        fi
        add mkalias ". /usr/local/bin/scripts/mkalias.sh"
        mkinstall
      fi
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--uninstall" ]
    then # uninstall alias for a script
    echo "Uninstalling..."
    if [ $# -eq 1 ]
      then

      echo "Are you sure you want to uninstall 'mkalias'?"
      select yn in "Yes" "No"
        do
        case $yn in
          Yes )
          if check "alias mkalias=" $file1
            then
            delete "mkalias" $file1
          fi
          if [ -f /usr/local/bin/scripts/mkalias.sh ]
            then
            sudo rm /usr/local/bin/scripts/mkalias.sh
          fi
          if [ -f /usr/local/man/man1/mkalias.1.gz ]
            then
            sudo rm /usr/local/man/man1/mkalias.1.gz
          fi
          souceFile
          echo "'mkalias' uninstalled successfully!"
          break;;
          No )
          echo "Abort! 'mkalias' is safe and sound!"
          break;;
        esac
      done
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--add" -o $1 = "-a" ]
    then # add alias to ~/.bash_aliases
    if [ $# -eq 3 ]
      then
      if check "alias $2=" $file1
        then
        echo "Alias '$2' already exists."
        echo "Use 'mkalias -update' to update it."
      else
        add $2 "$3"
        souceFile
        echo "Alias created!"
      fi
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--update" -o $1 = "-u" ]
    then
    echo "Use 'mkalias --update-value' to update value for a given alias"
    echo "Use 'mkalias --update-key' to update alias name"
    echo "For more information see 'mkalias' man page"
  elif [ $1 = "--update-value" -o $1 = "-uv" ]
    then # update alias value in ~/.bash_aliases
    if [ $# -eq 3 ]
      then
      if check "alias $2=" $file1
        then
        update-value $2 "$3"
        souceFile
        echo "Alias updated!"
      else
        update-value $2 "$3"
        souceFile
        echo "Alias created!"
      fi
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--update-key" -o $1 = "-uk" ]
    then # update alias key in ~/.bash_aliases
    if [ $# -eq 3 ]
      then
      if check "alias $2=" $file1
        then
        update-key $2 "$3"
        souceFile
        echo "Alias updated!"
      else
        echo "Error! There is no alias with the name '$2'"
      fi
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--delete" -o $1 = "-d" ]
    then # delete alias from ~/.bash_aliases
    if [ $# -eq 2 ]
      then
      if check "alias $2=" $file1
        then
        echo "Are you sure you want to delete alias '$2'?"
        select yn in "Yes" "No"
          do
          case $yn in
            Yes )
            delete $2
            souceFile
            echo "Alias '$2' deleted!"
            break;;
            No )
            echo "Alias '$2' not deleted!"
            break;;
          esac
        done
      else
        echo "Error! There is no alias named '$2'."
      fi
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--list" -o $1 = "-l" ]
    then # list aliases in $file1
    if [ $# -eq 1 ]
      then
      echo "Aliases in '$file1':"
      echo
      cat $file1
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  elif [ $1 = "--version" -o $1 = "-v" ]
    then # get 'mkalias' version
    if [ $# -eq 1 ]
      then
      echo "Version $version"
    else
      echo "Error! Wrong number of arguments passed to a script."
      errorInfo
    fi
  else
    echo "Error!"
    errorInfo
  fi

  # return to the folder from which the script was called
  cd "$OLDPWD"
  OLDPWD=$OLDPWDref

else
  echo "Error! Wrong number of arguments passed to a script."
  errorInfo
fi
