# mkalias

> `mkalias` is a simple CLI tool that helps you manage your custom shell aliases.

## Instalation

``` bash
# download mkalias
$ git clone git@bitbucket.org:Svetislav/mkalias.git

# install/update mkalias
$ . ./mkalias.sh --install

# uninstall mkalias
$ . ./mkalias.sh --uninstall
```

By installing this tool following things happen:

- If the file `.bash_aliases` doesn't exist in your home folder then it is created. All aliases you create with `mkalias` are stored in this file. At this point there is at least one alias in this file and that is an alias for `mkalias` itself.
- File `.bash_aliases` gets imported into `.bashrc` (if not already imported). If `.bashrc` doesn't exist in your home folder you will have to manually create it and import `.bash_aliases`. Instructions on how to do that are below.
- Script `mkalias.sh` is copied to /usr/local/bin/scripts (recommended place for your custom shell scripts).
- Man page `mkalias.1.gz` is copied to /usr/local/man/man1 (recommended place for your custom man pages).
- Feel free to delete mkalias folder since it is no longer needed.
- You are ready to go!

Uninstalling `mkalias` deletes `mkalias.sh` script and `mkalias.1.gz` man page from your system but doesn't affect `.bash_aliases` and `.bashrc` files - only alias for `mkalias` is removed from `.bash_aliases`. All aliases you created using `mkalias` will still be available even after uninstalling `mkalias`.

### Creating `.bashrc` file

If you do not have `.bashrc` file in your home folder (~) you will have to create it and import `.bash_aliases` into it. Here are the instructions on how to do that:

- Go to your home folder and create file called `.bashrc`
``` bash
$ cd ~
$ touch .bashrc
```
- Open `.bashrc` in a prefered editor and add these lines:
``` bash
# source .bash_aliases
if [ -f ~/.bash_aliases ]
  then
  . ~/.bash_aliases
fi
```
- Source `.bashrc`:
``` bash
$ . .bashrc
```
- That's it! Don't worry if you already created some aliases using `mkalias` before creating `.bashrc` file - they will be available right after you source `.bashrc`

## Usage

Instructions on how to use `mkalias` can be found in the man page:
``` bash
$ man mkalias
```
Using `mkalias` is quite simple, you can call `mkalias` from anywhere in the filesystem:

mkalias [OPTIONS] [ALIAS-NAME] [COMMAND]

### OPTIONS

``` bash
# add alias to ~/.bash_aliases

  --add, -a

# delete alias from ~/.bash_aliases. Prompt will appear asking for confirmation.

  --delete, -d

# list aliases stored in ~/.bash_aliases. Note that this doesn't list all available aliases. To list all aliases use native 'alias' command.

  --list, -l

# uninstall 'mkalias' from your machine.

  --uninstall

# update name of an alias from ~/.bash_aliases.

  --update-key, -uk

# update command of an alias from ~/.bash_aliases. If alias doesn't exist it will be created.

  --update-value, -uv

# get 'mkalias' version.

  --version, -v
```

### ALIAS-NAME

This is the name of an alias. Use only one word for the name, i.e. do not use whitespaces as this may lead to unexpected alias creation.

### COMMAND

Command for your alias. If this is set of commands use quotation marks ("" or '').

## Examples

``` bash
# Create an alias called 'll'
$ mkalias -a ll 'ls -l'

# Update name of an alias stored in ~/.bash_aliases, from 'll' to 'lla'. Alias command stays the same.
$ mkalias -uk ll lla

# Update command for an alias called 'll' stored in ~/.bash_aliases. If alias doesn't exist this command will create it.
$ mkalias -uv ll 'ls -la'

# Delete an alias called 'll'.
$ mkalias -d ll

# List all aliases stored in ~/.bash_aliases.
$ mkalias -l
```

### Useful aliases

Here are some aliases I use that can come in handy. If you like them, add them, do not hesitate!

``` bash
alias c='clear'
alias x='exit'
alias ll='ls -l --color=always'
alias lla='ls -la --color=always'
alias ..='cd ..'
alias ..2='cd ../..'
alias ..3='cd ../../..'
alias ..4='cd ../../../..'
alias ..5='cd ../../../../..'
alias app='xdg-open'
```

## Disclaimer

This CLI tool is a small project of mine that I only tested on my machine.
I do not guarantee that it will work as it should on your own machine and do not take any responsibility 
for any damage that it migth cause. That being said I advise you to use it with caution.

You are free to use it, change it so it fits your needs, and share it.

For any questions or suggestions fell free to contact me.
